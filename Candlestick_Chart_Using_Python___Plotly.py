#!/usr/bin/env python
# coding: utf-8

# # Candlestick Chart Using Python & Plotly
# #By- Aarush Kumar
# #Dated: May 19,2021

# In[6]:


import pandas as pd
import plotly.graph_objects as go


# In[7]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Candlestick Chart/cc.csv')
df


# In[8]:


df=df.set_index(pd.DatetimeIndex(df['Date'].values))
df


# In[10]:


fig=go.Figure(
    data=[
        go.Candlestick(
        x = df.index,
        low=df['Low'],
        high=df['High'],
        close=df['Close'],
        open=df['Open'],
        increasing_line_color='green',
        decreasing_line_color='red'
        )
        
    ])
fig.show()

