# Candlestick chart 
# By- Aarush Kumar
Data plot in candlestick form using Python & Pyplot
Candlesticks show that emotion by visually representing the size of price moves with different colors. Traders use the candlesticks to make trading decisions based on regularly occurring patterns that help forecast the short-term direction of the price. 
 Just like a bar chart, a daily candlestick shows the market's open, high, low, and close price for the day. The candlestick has a wide part, which is called the "real body." 
This real body represents the price range between the open and close of that day's trading. When the real body is filled in or black, it means the close was lower than the open. If the real body is empty, it means the close was higher than the open. 
Thankyou!

You may learn more about Candlechart by visiting:[Candlestick chart](https://www.investopedia.com/trading/candlestick-charting-what-is-it/)
